<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Hola</title>

    <style>
        body{
            width: 100vw;
            height: 100vh;
            display: flex;
            align-items: center;
            flex-direction: column;
            justify-content: center;
            background: linear-gradient(to bottom, #003E51 0%,#009CA6 100%);
            overflow: hidden;
        }

        .container{
            background-color: white;
            width: 500px;
            height: 300px;
            box-shadow: 4px 4px 4px 0px rgba(0,0,0,0.4);
            border-radius: 4px;
        }

        h1{
            box-sizing: border-box; 
            font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; 
            position: relative; 
            color: #3d4852; 
            font-size: 20px; 
            font-weight: bold; 
            text-align: left;
            margin-top: 20px;
            left: 20px;
        }

        .titulo{
            box-sizing: border-box; 
            font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; 
            position: relative; 
            color: #000000; 
            font-size: 30px; 
            font-weight: bold; 
            text-align: center;
            margin-top: 20px;
            margin-right: 35px;
        }

        p{
            box-sizing: border-box; 
            font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; 
            position: relative; 
            color: #E94320; 
            font-size: 14px;
            font-weight: bold;
            text-align: left;
            margin-top: 0px;
            left: 20px;
        }

        a{
            box-sizing: border-box; 
            font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; 
            position: relative; 
            -webkit-text-size-adjust: none;
            border-radius: 4px; 
            color: #fff; 
            display: inline-block; 
            overflow: hidden; 
            text-decoration: none; 
            background-color: #2d3748; 
            border-bottom: 8px solid #2d3748; 
            border-left: 18px solid #2d3748; 
            border-right: 18px solid #2d3748; 
            border-top: 8px solid #2d3748;
        }

        .button{
            display: flex;
            align-items: center;
            justify-content: center;
        }

        .text{
            box-sizing: border-box; 
            font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; 
            position: relative; 
            color: #000000; 
            font-size: 12px;
            font-weight: bold;
            line-height: 10%;
            text-align: left;
            left: 20px;
        }

        @media  only screen and (max-width: 500px) {
            .container{
                width: 260px;
            }
            p{
                left: 15px;
            }
            h1{
                left: 15px;
            }
            .text{
                left: 15px;
            }
        }

    </style>

</head>
<body>
    <h1 class="titulo">MLSC Servicios Legales Y Contables</h1>
    <div class="container">
        <h1>Información de la cita</h1>
        <p>Nombre del cliente</p>
        <p class="text">{{ $cita->nombre_persona }}</p>
        <p>Telefono del cliente</p>
        <p class="text">{{ $cita->numero_persona }}</p>
        <p>Correo Electronico</p>
        <p class="text">{{ $cita->correo_persona }}</p>
        @if($cita->tipo_cita)
        <p>Tipo de Cita</p>
        <p class="text">Legal</p>
        @else
        <p>Tipo de Cita</p>
        <p class="text">Contable</p>
        @endif
        <p>Fecha y hora de la cita</p>
        <p class="text">{{ $fecha }}</p>
    </div>
</body>
</html>