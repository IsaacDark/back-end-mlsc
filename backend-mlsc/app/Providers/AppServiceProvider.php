<?php

namespace App\Providers;

use Carbon\Carbon;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //setlocale(LC_ALL,'es_ES.UTF-8');
        setlocale(LC_TIME,$this->app->getLocale());
        Carbon::setLocale($this->app->getLocale());
    }
}
