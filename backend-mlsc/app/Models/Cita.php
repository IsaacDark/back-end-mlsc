<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cita extends Model
{
    protected $fillable = [
        'tipo_cita',
        'nombre_persona',
        'correo_persona',
        'numero_persona',
        'fecha_cita',
        'hora_cita'

    ];   
}
