<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Cita;
use Carbon\Carbon;

class LegalMailable extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     * 
     */
    public $fecha;

    public $subject = "Cita Digital";
    
    public $cita;

    public function __construct(Cita $cita)
    {
        Carbon::setUtf8(true);
        
        $this->cita = $cita;

        $dt = new Carbon($this->cita->fecha_cita);

        $string =  substr($this->cita->hora_cita, 0,2);

        $dt->hour = $string;

        $this->fecha = $dt->formatLocalized('%A %d de %B de %Y a las %H:%M');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.agenda');
    }
}
