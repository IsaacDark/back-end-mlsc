<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\LegalMailable;
use App\Models\Cita;

class CitaController extends Controller
{
    //
    public function enviarCorreo(Request $request){

        $cita = new Cita([
            'tipo_cita' => $request->tipo_cita,
            'nombre_persona' => $request->nombre_persona,
            'correo_persona' => $request->correo_persona,
            'numero_persona' => $request->numero_persona,
            'fecha_cita' => Carbon::parse($request->fecha_cita)->format('d-m-Y'),
	        'hora_cita' => $request->hora_cita
        ]);


        /* $cita = new Cita([
            'tipo_cita' => '0',
            'nombre_persona' => 'Holi moncho :3',
            'correo_persona' => 'juiroes@hotmail.com',
            'numero_persona' => 'ya se puede enviar pero :v',
            'fecha_cita' => '21-09-22',
            'hora_cita' => '10:00'
        ]); */

        /* $array = [
            "manuel@mlsc.mx"
        ];

        array_push($array, $cita->correo_persona);

        if($cita->tipo_cita){
            array_push($array, "jmlopez@mlsc.mx");
        } */


        $array = [
            "juiroes@protonmail.com"
        ];

        array_push($array, $cita->correo_persona);
        /*
        if($cita->tipo_cita){
            array_push($array, "Contable@mlsc.mx");
        } */


        
        foreach ($array as $recipient) {
            Mail::to($recipient)->send(new LegalMailable($cita));
        }

        //Mail::to('juanito@gmail.com')->send(new LegalMailable($cita));

        $cita->save();
        
        return response()->json(["data"=> "Cita registrada Correctamente"]);
    }


    public function prueba(Request $request){
        
	/* $cita = new Cita([
            'tipo_cita' => $request->tipo_cita,
            'nombre_persona' => $request->nombre_persona,
            'correo_persona' => $request->correo_persona,
            'numero_persona' => $request->numero_persona,
            'fecha_cita' => Carbon::parse($request->fecha_cita)->format('d-m-Y'),
		'hora_cita' => $request->hora_cita	
        ]);

        $cita->save(); */
        
        $dt = new Carbon('19-09-2021');

        $hora = "21:00";

        $string =  substr($hora, 0,2);

        $dt->hour = $string;

        return $dt->formatLocalized('%A %d de %B de %Y a las %H:%M');
        

        //return $dt->formatLocalized('%l %j \\of %F \\of %Y %H:%i %A');

        return "cita registrada";
    }


    public function horasDisponibles(Request $request){

        $horasDisponibles = [
            '09:00',
            '10:00',
            '11:00',
            '12:00',
            '13:00',
            '14:00',
            '15:00',
            '16:00',
            '17:00',
            '18:00',
            '19:00',
            '20:00'
        ];

        $data = $request->all();

        $fecha = Carbon::parse($data['fecha'])->format('d-m-Y');

	

        $diaSeleccionado = Cita::where('fecha_cita',$fecha)->get()->toArray();

	

        $horasOcupadas = array_map(function($value){
            
            return $value['hora_cita'];
        }, $diaSeleccionado);


        $horasLibres = array_values(array_diff($horasDisponibles, $horasOcupadas));

        /* if(empty($horasLibres)){
            return "no hay horas disponibles";
        } */

        return response()->json(["data"=> $horasLibres]);

    }
}

