<?php
use Carbon\Carbon;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CitaController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    Carbon::setUtf8(true);

    $dt = Carbon::Now();

    $dt->setDay(22);


    $data = Carbon::parse('2021-9-22')->format('d-m-Y');
    
    return $dt->formatLocalized('%A %d de %B de %Y a las %H:%M');
});





